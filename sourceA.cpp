﻿#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}



int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open(" FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name text)", NULL, 0, &zErrMsg);//make new table un the file
	rc = sqlite3_exec(db, "insert into people(name) values(\"Shalomi Avitan\")", NULL, 0, &zErrMsg);///all the insert insert to table data and the id is count.
	rc = sqlite3_exec(db, "insert into people(name) values(\"Alex\")", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values(\"Roei Ben david\")", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "update people Set name = \"Shalev\" Where id = 3", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "Select * from people", callback, 0, &zErrMsg);
	printTable();//print the table
	clearTable();//clear rhe all old table
	system("Pause");

}