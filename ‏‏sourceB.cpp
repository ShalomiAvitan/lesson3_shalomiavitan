﻿#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
unordered_map<string, vector<string>> results;


void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

//The function that receives the ID of the buyer, and the ID of the vehicle, and checks whether the purchase can be made
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	clearTable();
	string available;
	string carPrice;
	int carPrice2;
	string balanceBuyer;
	int balanceBuyer2;
	int balanceNew;
	string messgeBuyer;
	string messgeAvailable;

	available = "Select available from cars Where id =" + to_string(carid);//get the full SQL
	sqlite3_exec(db, available.c_str(), callback, nullptr, &zErrMsg);//send the SQL
	if (results["available"][0] == "0") //check if the car available(if this if true the car dint available)
	{
		return false;
	}


	clearTable();
	carPrice = "Select price from cars Where id =" + to_string(carid);//get the full SQL
	sqlite3_exec(db, carPrice.c_str(), callback, nullptr, &zErrMsg);//send the SQL
	carPrice2 = stoi(results["price"][0]);//get answer in int - this price of car
	clearTable();
	balanceBuyer = "Select balance from accounts Where id =" + to_string(buyerid);
	sqlite3_exec(db, balanceBuyer.c_str(), callback, nullptr, &zErrMsg);
	balanceBuyer2 = stoi(results["balance"][0]);//get answer in int - this the many of the buyer 
	if (carPrice2 > balanceBuyer2)// if the car costs more than the amount of money the buyer
	{
		return false;
	}
	else
	{
		balanceNew = balanceBuyer2 - carPrice2;//taking down money from the buyer
		messgeBuyer = "update accounts set balance =" + to_string(balanceNew) + "Where id =" + to_string(buyerid);//get the full SQL
		sqlite3_exec(db, messgeBuyer.c_str(), callback, nullptr, &zErrMsg);//send the SQL

		clearTable();

		messgeAvailable = "update cars set available =" + to_string(0) + "Where id=" + to_string(carid);//get the full SQL
		sqlite3_exec(db, messgeAvailable.c_str(), callback, nullptr, &zErrMsg);//send the SQL

		return true;
	}

}


//The function allows transferring money from account 1 to account 2.
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int blance = 0;
	int blance2 = 0;
	int money = 0;
	string balanceFrom;
	string balanceTo;
	string messgeFrom;
	string messgeTo;
	clearTable();
	balanceFrom = "Select balance from accounts Where id=" + to_string(from);//get the full SQL
	sqlite3_exec(db, balanceFrom.c_str(), callback, nullptr, &zErrMsg);//send the SQL
	money = stoi(results["balance"][0]);//switch the string with a int -  get the balance of the user
	if (amount > money)// check if have enough many
	{
		return false;
	}
	clearTable();
	balanceTo = "Select balance from accounts Where id=" + to_string(to); // get the full SQL
	sqlite3_exec(db, balanceTo.c_str(), callback, nullptr, &zErrMsg);//send the SQL
	blance = amount + stoi(results["balance"][0]); // get answer in int - get the sum blance
	blance2 = money - amount;//Reduces the amount of money
	messgeFrom = "update accounts set balance=" + to_string(blance) + "Where id=" + to_string(to); // get the full SQL
	sqlite3_exec(db, messgeFrom.c_str(), callback, nullptr, &zErrMsg);//send the SQL
	messgeTo = "update accounts set balance=" + to_string(blance2) + "Where id=" + to_string(from); // get the full SQL
	sqlite3_exec(db, messgeTo.c_str(), callback, nullptr, &zErrMsg);//send the SQL

	return true;
}


int main()
{
	int rc;
	sqlite3 * db;
	char *zErrMsg = 0;
	bool flag = true;
	rc = sqlite3_open("carsDealer.db", &db);


	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	if (carPurchase(1, 1, db, 0))//example to Purchase incomplete
	{
		cout << "Purchase completed" << endl;
	}
	else
	{
		cout << "Purchase incomplete" << endl;
	}

	if (carPurchase(1, 18, db, 0))//example to Purchase completed
	{
		cout << "Purchase completed" << endl;
	}
	else
	{
		cout << "Purchase incomplete" << endl;
	}

	if (carPurchase(12, 23, db, 0))//example to Purchase completed
	{
		cout << "Purchase completed" << endl;
	}
	else
	{
		cout << "Purchase incomplete" << endl;
	}

	if (balanceTransfer(1, 2, 100, db, 0))
	{
		cout << "completed" << endl;
	}
	else
	{
		cout << "incomplete" << endl;
	}

	system("Pause");

	return 0;
}